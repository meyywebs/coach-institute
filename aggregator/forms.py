from django import forms
from .models import SignUp,Address,Institution,Subject
import re

class SignUpForm(forms.ModelForm):
	class Meta:
		model = SignUp
		fields = ['full_name','email']

	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base,provider = email.split("@")
		domain,extension = provider.split(".")

		if not domain == 'gmail':
			raise forms.ValidationError("accepts only gmail")
		if not extension == 'com':
			raise forms.ValidationError("accepts only .com")

		return email

	def clean_full_name(self):
		full_name = self.cleaned_data.get('full_name')
		name_check = re.compile(r"[^A-Za-zs.]")

		if name_check.search(full_name):
			raise forms.ValidationError("accepts only characters")

		return full_name

class AddressForm(forms.ModelForm):
	
	class Meta:
		model = Address
		exclude = ()

class InstitutionForm(forms.ModelForm):
	
	class Meta:
		model = Institution
		exclude = ('address',)

	def clean_phone_no(self):
		phone_no = str(self.cleaned_data.get('phone_no'))
		reg_check = re.compile(r"^[6-9]\d{9}$")
		if not reg_check.search(phone_no):
			raise forms.ValidationError("invalid phone number")
		return phone_no
		
	def clean_website(self):
		website = str(self.cleaned_data.get('website'))
		reg_check = re.compile(r"^w{3}\.(.+)(\.com)$")
		if not reg_check.search(website):
			raise forms.ValidationError("website format www.example.com")
		return website
	

class InstitutionEditForm(forms.ModelForm):
	
	class Meta:
		model = Institution
		exclude = ('address','Institution_name')

	def clean_phone_no(self):
		phone_no = str(self.cleaned_data.get('phone_no'))
		reg_check = re.compile(r"^[6-9]\d{9}$")
		if not reg_check.search(phone_no):
			raise forms.ValidationError("invalid phone number")
		return phone_no

	def clean_website(self):
		website = str(self.cleaned_data.get('website'))
		reg_check = re.compile(r"^w{3}\.(.+)(\.com)$")
		if not reg_check.search(website):
			raise forms.ValidationError("website format www.example.com")
		return website

class SubjectForm(forms.ModelForm):
	
	class Meta:
		model = Subject
		exclude = ()

