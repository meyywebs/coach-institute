# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class SignUp(models.Model):
	email = models.EmailField()
	full_name = models.CharField(max_length=120,blank=False,null=True)
	timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
	updated = models.DateTimeField(auto_now_add=False,auto_now=True)

	def __unicode__(self):
		return self.email

class Address(models.Model):
	door_no = models.CharField(max_length=120,blank=False,null=False)
	floor_no = models.IntegerField(blank=False,null=False)
	street_name = models.CharField(max_length=120,blank=False,null=False)
	town_name = models.CharField(max_length=120,blank=False,null=False)
	city_name = models.CharField(max_length=120,blank=False,null=False)
	state_name = models.CharField(max_length=120,blank=False,null=False)
	country_name = models.CharField(max_length=120,blank=False,null=False)
	timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
	updated = models.DateTimeField(auto_now_add=False,auto_now=True)

	def details(self):
		return {'door_no':self.door_no,
				'floor_no':self.floor_no,
				'street_name':self.street_name,
				'town_name':self.town_name,
				'city_name':self.city_name,
				'state_name' :self.state_name,
				'country_name':self.country_name,
				}

	def __unicode__(self):
		display = self.door_no+' '+str(self.floor_no)+' '+self.street_name+' '+self.town_name+' '+self.city_name+' '+self.state_name+' '+self.country_name
		return display

class Subject(models.Model):
	subject_name = models.CharField(primary_key=True,max_length=120,blank=False,null=False)
	timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
	updated = models.DateTimeField(auto_now_add=False,auto_now=True)

	def __str__(self):
		return self.subject_name

class Institution(models.Model):
	#address, contact number, website, subjects taught, 
	address = models.OneToOneField(Address)
	phone_no = models.BigIntegerField(blank=False,null=False)
	website = models.CharField(max_length=120,blank=False,null=False)
	subjects = models.ManyToManyField(Subject)
	email = models.EmailField()
	Institution_name = models.CharField(unique=True,max_length=120,blank=False,null=True)
	timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
	updated = models.DateTimeField(auto_now_add=False,auto_now=True)

	def details(self):
		return {'address':self.address,
				'phone_no':self.phone_no,
				'website':self.website,
				'subjects':self.subjects.all,
				'email' :self.email,
				'Institution_name':self.Institution_name
				}

	def __unicode__(self):
		return self.id