# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,Http404,redirect

from django.contrib import messages


from django.contrib.auth.models import User
from .forms import AddressForm,InstitutionForm,SubjectForm,InstitutionEditForm
from django.forms.formsets import formset_factory
from .models import Institution

# Create your views here.

def add_subject(request):
	title = 'Add Subject'
	Subjectform = SubjectForm(request.POST or None)
	context = {
		"template_title":title,
		"Subjectform":Subjectform,
				}
	if Subjectform.is_valid():
		if not request.user.is_active:
			messages.success(request, 'session timed out!')
			return redirect('home')
		elif request.user.is_authenticated() and request.user.is_staff:
			Subjectform.save();
			messages.success(request, 'successfully added subject!')
			return redirect('home')
	return render(request,"add_subject.html",context)

def home (request):
	title = 'Welcome'

	Addressform = AddressForm(request.POST or None)
	InstitutionFormSet = formset_factory(InstitutionForm, extra=0, min_num=1, validate_min=True)
	FormSet = InstitutionFormSet(request.POST or None)
	context = {
		"template_title":title,
		"Addressform":Addressform,
		"Institutionform":FormSet,
			}
	if request.user.is_authenticated() and request.user.is_staff:
		if Addressform.is_valid() and FormSet.is_valid():
			if not request.user.is_active:
				messages.success(request, 'session timed out!')
				return redirect('home')
			address = Addressform.save()
			for inline_form in FormSet:
				if inline_form.cleaned_data:
					Institute = inline_form.save(commit=False)
					Institute.address = address
					Institute.save()
					inline_form.save_m2m()
					messages.success(request, 'successfully added!')
			return redirect('home')

		Institutions = Institution.objects.all()
		workers = User.objects.all().filter(is_staff=False,is_active=True).order_by('-date_joined')
		context = {
			"Addressform":Addressform,
			"Institutionform":FormSet,
			"logged_in_as": "You are logged in as admin",
			"template_title":title,
			"Institutions":Institutions,
			"workers":workers,
		}

	elif request.user.is_authenticated() and not request.user.is_staff:
		Institutions = Institution.objects.all()
		context = {
			"logged_in_as": "You are logged in as viewer",
			"template_title":title,
			"Institutions":Institutions,
		}
	return render(request,"home.html",context)

def institute_edit(request,param): 
	if request.user.is_authenticated() and request.user.is_staff:
		Institutiondetail = Institution.objects.get(Institution_name=param)
		Addressform = AddressForm(request.POST or None, initial=Institutiondetail.details()['address'].details())
		InstitutionFormSet = formset_factory(InstitutionEditForm, extra=0, min_num=1, validate_min=True)
		FormSet = InstitutionFormSet(request.POST or None, initial=[Institutiondetail.details()])
		context = {
			"Addressform":Addressform,
			"Institutionform":FormSet,
				}
		if 'delete' in request.POST:
			if Addressform.is_valid() and FormSet.is_valid():
				if not request.user.is_active:
					messages.success(request, 'session timed out!')
					return redirect('home')
				Institutiondetail.delete()
				messages.success(request, 'successfully deleted!')
				return redirect('home')
		else:
			if Addressform.is_valid() and FormSet.is_valid():
				if not request.user.is_active:
					messages.success(request, 'session timed out!')
					return redirect('home')
				address = Addressform.save()
				for inline_form in FormSet:
					if inline_form.cleaned_data:
						inline_form.save(commit=False)
						Institutiondetail.address = address
						Institutiondetail.email = inline_form.cleaned_data.get('email')
						Institutiondetail.phone_no = inline_form.cleaned_data.get('phone_no')
						Institutiondetail.website = inline_form.cleaned_data.get('website')
						Institutiondetail.subjects = inline_form.cleaned_data.get('subjects')
						Institutiondetail.save()
						messages.success(request, 'successfully updated!')
				return redirect('home')
		return render(request,"institute_edit.html",context)
	else:
		raise Http404

